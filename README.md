# 好得涂

好得涂是一款绘画软件。

版权所有 (C) 2022-2025 吴奕茗

了解更多: https://ChengduLittleA.com/ourpaint

赞助：https://patreon.com/ChengduLittleA

好得涂 采用 GNU GPL v3 许可证，Noto 字体采用 SIL Open Font 许可证
您将在源代码文件夹找到许可证的具体信息。

Resources 中的启动画面采用 Creative Commons Attribution-NonCommercial 2.0 Generic (CC BY-NC 2.0) 协议。
您不能将这些图片用作商业用途，但您可以联系作者以了解如印刷品等其他产品的授权。

随 Our Paint 发行版附带的默认笔刷文件采用 Creative Commons Attribution-NonCommercial 2.0 Generic (CC BY-NC 2.0) 协议。

## 启动画面信息

该版本好得涂的启动画面作者是 吴奕茗。

联系作者：https://ChengduLittleA.com | ChengduLittleA@Outlook.com

## default_brushes.udf 信息

default_brushes.udf 的作者是 吴奕茗。

联系作者：https://ChengduLittleA.com | ChengduLittleA@Outlook.com


-----------------

# Our Paint

Our Paint is a painting application.

Copyright (C) 2022-2025 Wu Yiming

Learn more about LaGUI: https://ChengduLittleA.com/lagui

Support the development: https://patreon.com/ChengduLittleA

Our Paint is licensed with GNU GPL v3, and Noto fonts are licensed with SIL Open Font license.
You should be able to find details about the license in the source code directory.

The splash screen images under Resources are licensed with Creative Commons Attribution-NonCommercial 2.0 Generic (CC BY-NC 2.0).
You can not use these images commercially, but you are free to contact the author for licensing info on other products such as prints.

The brushe files packed with Our Paint distribustion are licensed with Creative Commons Attribution-NonCommercial 2.0 Generic (CC BY-NC 2.0).

## Splash screen image image info

The artist of the splash screen image in this version of Our Paint is Wu Yiming.

Contact the artist: https://ChengduLittleA.com | ChengduLittleA@Outlook.com

## default_brushes.udf file info

The author of default_brushes.udf file is Wu Yiming.

Contact the author: https://ChengduLittleA.com | ChengduLittleA@Outlook.com

